# Technical Paper on Javascript

## Loops

- Loops are used to perform any repetitive set of tasks.

- ### for

  `for` loop is mostly used to iterate over the any iterable
  within a specified range.

  ```javascript
  for (let index = 0; index < iterable.length; index++) {
    // Repetitive task to be performed over the range 0 to length of the iterable
    console.log(index, iterable[index]);
  }
  ```

- ### forEach

  `forEach` loop emulates for loop. It is also used to iterate over an array of elements and perform actions specified in the callback function.

  ```javascript
  let array = [1, 2, 3, 4, 5];

  array.forEach((element) => {
    console.log(element);
  });

  /*
   Output:
   1
   2
   3
   4
   5
  */
  ```

- ### for .. in

  `for .. in` loop is used to iterate over an object and perform actions as per the need over keys and values

  ```javascript
  let object = { a: 1, b: { b1: 2.1, b2: 2.2 }, c: 3 };

  for (const key in object) {
    console.log(key, object[key]);
  }

  /*
   Output:
    a 1
    b { b1: 2.1, b2: 2.2 }
    c 3
  */
  ```

- ### for .. of

  `for .. of` loop is used to iterate over an array and perform actions as per the need over each element. It is used when there is no need to access index of elements.

  ```javascript
  let array = ["Ironman", "Hulk", "Captain America"];

  for (const element of array) {
    console.log(element);
  }

  /*
   Output:
    Ironman
    Hulk
    Captain America
  */
  ```

- ### while

  `while` loop is used to iterate and perform actions until certain conditions is/are achieved.

  ```javascript
  while (conditon) {
    // perform operations
  }
  ```

  ```javascript
  let element = 1;

  while (element < 5) {
    console.log(element);
    element++;
  }

  /*
    Output:
    1
    2
    3
    4
   */
  ```

## Pass by Reference and Pass by Value

- Parameters are always passed by value.

```javascript
let counter = 10;

function incrementCounterBy1(value) {
  value++;
  console.log("After increment", value);
}

console.log("Before execution of increment function", counter);

incrementCounterBy1(counter);

console.log("After execution of increment function", counter);

/*
    Output:
    Before execution of increment function 10
    After increment 11
    After execution of increment function 10
    */
```

- Pass by Reference (for objects and arrays):

```javascript
let counter = { value: 10 };

function incrementCounterBy1(counter) {
  counter.value++;
  console.log("After increment", counter.value);
}

console.log("Before execution of increment function", counter.value);
incrementCounterBy1(counter);
console.log("After execution of increment function", counter.value);

/*
Output:
Before execution of increment function 10
After increment 11
After execution of increment function 11
*/
```

## Array Methods

- ### Array.pop()

  Removes the last element from an array and returns that element.

  ```javascript
  let fruits = ["apple", "banana", "mango"];
  fruits.pop(); // Removes "mango"
  console.log(fruits); // Output: ["apple", "banana"]
  ```

- ### Array.push()

  Adds one or more elements to the end of an array and returns the new length of the array.

  ```javascript
  let fruits = ["apple", "banana"];
  fruits.push("orange"); // Adds "orange" to the end
  console.log(fruits); // Output: ["apple", "banana", "orange"]
  ```

- ### Array.concat()

  Adds one or more elements to the end of an array and returns the new length of the array.

  ```javascript
  let fruits = ["apple", "banana"];
  let moreFruits = ["orange", "grape"];
  let allFruits = fruits.concat(moreFruits); // Combines both arrays
  console.log(allFruits); // Output: ["apple", "banana", "orange", "grape"]
  ```

- ### Array.slice()

  Extracts a section of an array and returns a new array.

  ```javascript
  let fruits = ["apple", "banana", "orange", "grape", "kiwi"];
  let citrus = fruits.slice(2, 4); // Extracts from index 2 to 4 (exclusive)
  console.log(citrus); // Output: ["orange", "grape"]
  ```

- ### Array.splice()

  Changes the contents of an array by removing or replacing existing elements and/or adding new elements.

  ```javascript
  let fruits = ["apple", "banana", "orange", "grape"];
  fruits.splice(2, 1, "kiwi"); // Removes 1 element from index 2 and adds "kiwi"
  console.log(fruits); // Output: ["apple", "banana", "kiwi", "grape"]
  ```

- ### Array.join()

  Joins all elements of an array into a string.

  ```javascript
  let fruits = ["apple", "banana", "orange"];
  let fruitString = fruits.join(", "); // Joins with comma and space
  console.log(fruitString); // Output: "apple, banana, orange"
  ```

- ### Array.flat()

  Creates a new array with all sub-array elements concatenated into it recursively up to the specified depth.

  ```javascript
  let nestedArray = [1, 2, [3, 4, [5, 6]]];
  let flatArray = nestedArray.flat(2); // Flattens nested arrays up to depth 2
  console.log(flatArray); // Output: [1, 2, 3, 4, 5, 6]
  ```

- ### Array.find()

  Returns the first element in the array that satisfies the provided testing function. Otherwise, it returns undefined.

  ```javascript
  let numbers = [10, 20, 30, 40, 50];
  let found = numbers.find((num) => num > 25);
  console.log(found); // Output: 30
  ```

- ### Array.indexOf()

  Returns the first index at which a given element can be found in the array, or -1 if it is not present.

  ```javascript
  let fruits = ["apple", "banana", "orange", "apple"];
  let index = fruits.indexOf("apple");
  console.log(index); // Output: 0
  ```

- ### Array.includes()

  Determines whether an array includes a certain value among its entries, returning true or false as appropriate.

  ```javascript
  let fruits = ["apple", "banana", "orange"];
  let hasBanana = fruits.includes("banana");
  console.log(hasBanana); // Output: true
  ```

- ### Array.findIndex()

  Returns the index of the first element in the array that satisfies the provided testing function. Otherwise, it returns -1.

  ```javascript
  let numbers = [10, 20, 30, 40, 50];
  let index = numbers.findIndex((num) => num > 25);
  console.log(index); // Output: 2
  ```

- ### Array.forEach()

  Executes a provided function once for each array element.

  ```javascript
  let numbers = [1, 2, 3];
  numbers.forEach((num) => console.log(num * 2)); // Output: 2, 4, 6
  ```

- ### Array.filter()

  Creates a new array with all elements that pass the test implemented by the provided function.

  ```javascript
  let numbers = [1, 2, 3];
  numbers.forEach((num) => console.log(num * 2)); // Output: 2, 4, 6
  ```

- ### Array.map()

  Creates a new array populated with the results of calling a provided function on every element in the calling array.

  ```javascript
  let numbers = [1, 2, 3];
  let doubledNumbers = numbers.map((num) => num * 2);
  console.log(doubledNumbers); // Output: [2, 4, 6]
  ```

- ### Array.reduce()

  Executes a reducer function on each element of the array, resulting in a single output value.

  ```javascript
  let numbers = [1, 2, 3, 4];
  let sum = numbers.reduce((acc, curr) => acc + curr, 0);
  console.log(sum); // Output: 10
  ```

- ### Array.sort()

  Sorts the elements of an array in place and returns the sorted array.

  ```javascript
  let fruits = ["banana", "apple", "orange"];
  fruits.sort();
  console.log(fruits); // Output: ["apple", "banana", "orange"]
  ```

- ### Array Methods Chaining

  Chaining allows you to combine multiple array methods in a single statement, where the output of one method becomes the input of the next.

  ```javascript
  let numbers = [1, 2, 3, 4, 5];
  let result = numbers
    .filter((num) => num % 2 === 0) // Filter even numbers
    .map((num) => num * 2) // Double each even number
    .reduce((acc, curr) => acc + curr, 0); // Sum up the doubled even numbers
  console.log(result); // Output: 12 (2*2 + 4*2)
  ```

## String methods

- String methods allow you to manipulate and work with text data.
- Strings in JavaScript are immutable, meaning their contents cannot be changed after they are created.
- There are no direct mutable methods for strings

### Immutable Methods

- `trim()`:
  Removes whitespace from both ends of a string. This method mutates the string.

```javascript
let str = "  hello  ";
str = str.trim();
console.log(str); // Output: "hello"
```

- `replace()`:
  Returns a new string with some or all matches of a pattern replaced by a replacement.

```javascript
let str = "hello";
console.log(str.replace("hello", "hi")); // Output: "hi"
console.log(str); // Output: "hello" (original string remains unchanged)
```

- `toString()`:
  Returns the value of a string object. This method does not mutate the string.

```javascript
let str = new String("hello");
console.log(str.toString()); // Output: "hello"
```

## Object methods

### Immutable Methods

- `Object.keys()`:
  Returns an array of a given object's own enumerable property names

```javascript
const person = { name: "John", age: 30 };
const keys = Object.keys(person);
console.log(keys); // Output: ["name", "age"]
```

- `Object.values()`:
  Returns an array of a given object's own enumerable property values.

```javascript
const person = { name: "John", age: 30 };
const values = Object.values(person);
console.log(values); // Output: ["John", 30]
```

- `Object.entries()`:
  Returns an array of a given object's own enumerable property values.

```javascript
const person = { name: "John", age: 30 };
const entries = Object.entries(person);
console.log(entries); // Output: [["name", "John"], ["age", 30]]
```

### Mutable Methods

- `Object.assign()`:
  Copies the values of all enumerable own properties from one or more source objects to a target object and returns the target object.

```javascript
let obj1 = { a: 1, b: 2 };
let obj2 = { b: 3, c: 4 };
Object.assign(obj1, obj2);
console.log(obj1); // Output: { a: 1, b: 3, c: 4 }
```

- `Object.defineProperty()`:
  Adds a new property to an object, or modifies an existing one, with the specified descriptor.

```javascript
let obj = {};
Object.defineProperty(obj, "name", { value: "John", writable: true });
console.log(obj.name); // Output: "John"
```

# Hoisting

- Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their containing scope during the compilation phase.

- `var` :
  Variables declared with var are hoisted to the top of their scope and initialized with a value of undefined. Assignments are not hoisted.

```javascript
console.log(x); // Output: undefined
var x = 5;
```

- `let and const` :
  Variables declared with let and const are hoisted to the top of their scope but are not initialized. They are in a "temporal dead zone" until execution reaches their declaration.

```javascript
console.log(y); // Error: Cannot access 'y' before initialization
let y = 10;
```

- Functions :
  Function declarations are hoisted in their entirety, meaning both the function name and its implementation are hoisted to the top of their scope.

  ```javascript
  someFunction(); // Output: "Hello, world!"
  function someFunction() {
    console.log("Hello, world!");
  }
  ```

# Scopes

- Scopes define the visibility and lifetime of variables and parameters. JavaScript has function-level and block-level scopes.

```javascript
function example() {
  var localVar = 10;
  console.log(localVar);
}
example(); // Output: 10
console.log(localVar); // Error: localVar is not defined
```

# Closures

- Closures allow functions to access variables from an outer scope even after the outer function has finished executing.

```javascript
function outer() {
  var x = 5;
  function inner() {
    console.log(x);
  }
  return inner;
}
var closureFunc = outer();
closureFunc(); // Output: 5
```

# Higher Order Functions

- Higher-order functions are functions that either take other functions as arguments or return functions.

```javascript
function higherOrder(func) {
  console.log("Executing the higher-order function");
  func();
}
function sayHello() {
  console.log("Hello!");
}
higherOrder(sayHello); // Output: "Executing the higher-order function" "Hello!"
```


# Best Practices
- Best practices in JavaScript refer to guidelines and conventions for writing clean, efficient, and maintainable code.
- Use of meaningful variable and function names, follow consistent coding style, avoid global variables, etc.

# Debugging 

- Debugging is the process of identifying and fixing errors or bugs in a program.
- Using browser developer tools, console.log statements, breakpoints, and debugging tools like VS Code debugger.